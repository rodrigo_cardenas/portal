<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/getDatosRut', 'HomeController@getDatosRut');
Route::get('/aplicativos', 'HomeController@aplicativos')->name('aplicativos');
Route::get('getParemetrosHosp', 'HomeController@getParemetrosHosp');
Route::get('getParemetrosPab', 'HomeController@getParemetrosPab');
Route::get('getParemetrosEquipoPab', 'HomeController@getParemetrosEquipoPab');
Route::get('getParemetrosCom', 'HomeController@getParemetrosCom');
Route::get('getParemetrosNot', 'HomeController@getParemetrosNot');
Route::get('/resetPass', 'HomeController@resetPass');
Route::get('/agregarSistemaMasivo', 'UserController@agregarSistemaMasivo');
Route::get('/agregarServicioMasivo', 'UserController@agregarServicioMasivo');

Route::get('user/editMe/', 'UserController@editMe');
Route::get('user/editFav/{tipo}/{sistema}', 'UserController@editFav');
Route::get('user/deleteUserSistema/{sistema}', 'UserController@deleteUserSistema');
Route::get('user/showByRut/{rut}', 'UserController@showByRut');
Route::get('user/destroy/{user}', 'UserController@destroy');
Route::resource('user', 'UserController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
