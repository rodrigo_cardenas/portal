@extends('adminlte::page')

@section('title', 'Usuarios')

@section('content_header')
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Usuarios</h3>
                    <div class="card-tools">
                        <a href="{{url('/agregarSistemaMasivo')}}" class="btn btn-xs btn-primary">Agregar Sistema Masivo</a>
                        <a href="{{url('/agregarServicioMasivo')}}" class="btn btn-xs btn-warning">Agregar Servicio Masivo</a>
                        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    {{-- filtros --}}
                    <form class="form-horizontal">
                        <div class="form-group row">
                            <div class="col-sm-5">
                            <input type="text" name="nombre" class="form-control" value="{{ request()->nombre }}" id="inputEmail3" placeholder="Nombre">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="rut" class="form-control" value="{{ request()->rut }}" id="inputEmail3" placeholder="Rut">
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-info">Filtrar</button>
                            </div>
                        </div>
                    </form>
                    {{-- tabla usuarios --}}
                    <table class="table table-striped table-hover" id="tableUsers">
                        <thead>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Rut</th>
                            <th>Fecha Creacion</th>
                            <th>Sistemas</th>
                            <th></th>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->nombre }}</td>
                                <td>{{ $user->rut }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>- {!! $user->sistemas->pluck('tx_descripcion')->implode('<br>- ') !!}</td>
                                <td>
                                    <a href="{{ url('user/destroy', $user->id)}}" class="btn btn-danger btn-xs" onclick="return confirm('Estas seguro de eliminar este usuario?');" title="Eliminar"><i class="fa fa-trash"></i></a>
                                    <a href="{{ url('user',$user->id)}}/edit" class="btn btn-info btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    {{ $users->appends(request()->query())->links() }}
                    
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
@stop

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
@include('sweet::alert')
<script>
    $('.brand-image').removeClass('elevation-3'); 
    $('.brand-image').removeClass('img-circle'); 
    $('.brand-text').css('color', '#10879a'); 
    $('[data-toggle="tooltip"]').tooltip();
    
    $('.table').DataTable({
        "paging": false,
        "lengthChange": true,
        "searching": false,
        "ordering": true,
        "info": false,
        "autoWidth": true,
        "language": {"url": "{{url('/')}}/js/dataTables/Spanish.json"},
    });
</script>
@stop