@extends('adminlte::page')

@section('title', 'Usuarios')

@section('content_header')
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Usuarios</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    {{-- filtros --}}
                    <form class="form-horizontal">
                        <div class="form-group row">
                            <div class="col-sm-5">
                                <select name="sistema" id="sistemas" required class="form-control select2">
                                    <option></option>
                                    @foreach($sistemas as $id => $sistema)
                                        <option value="{{ $id }}" {{ (isset($event) && $event->sistema ? $event->sistema->id : old('venue_id')) == $id ? 'selected' : '' }}>{{ $sistema }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select name="id_perfil" id="perfil" required class="form-control select2">
                                    <option></option>
                                    @foreach($perfiles as $id => $perfil)
                                        <option value="{{ $id }}" >{{ $perfil }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-info">Asignar</button>
                            </div>
                        </div>
                    </form>
                    
                    
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
@stop

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
@include('sweet::alert')
<script>
    $('.brand-image').removeClass('elevation-3'); 
    $('.brand-image').removeClass('img-circle'); 
    $('.brand-text').css('color', '#10879a'); 
    $('[data-toggle="tooltip"]').tooltip();
    
    
    $('#sistemas').select2({
        placeholder: "Seleccione Sistemas",
        width: "100%"
    });
    $('#perfil').select2({
        placeholder: "Seleccione Sistemas",
        width: "100%"
    });
    
</script>
@stop