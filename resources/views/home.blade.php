@extends('adminlte::page')

@section('title', 'Portal')

@section('content_header')
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Bienvenido(a),<b style="color:rgb(54, 175, 195)"> {{ Auth::user()->nombre }}</b> <small><b>IP:</b> {{ Request::ip() }}</small></h3> 
        <marquee class="float-right" style="width: 30%;" id="info"> 
            @if ($informaciones)
                <i class="fa {{ ($informaciones->tipo == 'Alerta') ? 'fa-exclamation-circle' : 'fa-info-circle'}}" style="color:red"></i> 
                 <strong>{{ $informaciones->tx_titulo }}</strong>: {{ $informaciones->tx_descripcion }}
            @else
                <i class="fa fa-arrow-circle-right"></i> 
                Este es el nuevo PortalHSJD con mejoras en diseño y usabilidad. Además, ahora podrás administrar tus sistemas, agregar tus favoritos y editar tu cuenta. 😉
            @endif
        </marquee>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        {{-- @include('partials.infoNueva') --}}
        @include('partials.sistemasFavoritos')
        @include('partials.sistemasTable')
    </div>
    <!-- /.card-body -->
  </div>
  @include('partials.contactosModal')
@stop

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
@stop

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
@include('sweet::alert')
<script> 
	$('.brand-image').removeClass('elevation-3'); 
	$('.brand-image').removeClass('img-circle'); 
    $('.brand-text').css('color', '#10879a'); 
    $('[data-toggle="tooltip"]').tooltip();
    
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": false,
      "autoWidth": true,
      "language": {"url": "{{url('/')}}/js/dataTables/Spanish.json"},
    });

    $( "#soporte" ).click(function() {
        $("#modalSoporte").modal('show')
    });

    function confirmar(url) {
        var opcion = confirm("Estas seguro de eliminar este sistema?");
        if (opcion == true) {
            location.href = url;
        } else {
            return false;
        }
    }
</script>
@stop