@extends('adminlte::master')

@section('adminlte_css_pre')
    <link rel="stylesheet" href="{{ asset('vendor/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@stop

@section('adminlte_css')
    @stack('css')
    @yield('css')
    <style>
        .login-page{
            background-image: linear-gradient(rgb(233, 236, 239), rgb(49, 190, 215));
        }

        @keyframes pulse {
            0% {
                transform: scale(0.95);
                /*box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.7);*/
            }
            70% {
                transform: scale(1);
                /*box-shadow: 0 0 0 10px rgba(0, 0, 0, 0);*/
            }
            100% {
                transform: scale(0.95);
                /*box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);*/
            }
        }

    </style>
@stop

@section('classes_body', 'login-page')

@php( $login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login') )
@php( $register_url = View::getSection('register_url') ?? config('adminlte.register_url', 'register') )
@php( $password_reset_url = View::getSection('password_reset_url') ?? config('adminlte.password_reset_url', 'password/reset') )
@php( $dashboard_url = View::getSection('dashboard_url') ?? config('adminlte.dashboard_url', 'home') )

@if (config('adminlte.use_route_url', false))
    @php( $login_url = $login_url ? route($login_url) : '' )
    @php( $register_url = $register_url ? route($register_url) : '' )
    @php( $password_reset_url = $password_reset_url ? route($password_reset_url) : '' )
    @php( $dashboard_url = $dashboard_url ? route($dashboard_url) : '' )
@else
    @php( $login_url = $login_url ? url($login_url) : '' )
    @php( $register_url = $register_url ? url($register_url) : '' )
    @php( $password_reset_url = $password_reset_url ? url($password_reset_url) : '' )
    @php( $dashboard_url = $dashboard_url ? url($dashboard_url) : '' )
@endif

@section('body')
    <div class="login-box">
        <div class="login-logo">
            <div class="card bg-primary" style="border-radius: 10px !important;">
                <div class="card-header">
                    <h3  style="text-align:center; float:none !important;">Bienvenido!! 👋</h3>
                    {{-- <h4 class="card-title" style="text-align:center; float:none !important;"> Este es el nuevo portal HSJDigital</h4>  --}}
                    <h4 class="card-title"> <b>Te informamos que tu clave son los primeros 4 dígitos de tu rut</b></h4> 
                    {{-- <h4 class="card-title"><small> <b>Una vez ingreses puedes cambiar tu clave</b></small> </h4>  --}}
                </div>
            </div>
            <div class="card bg-warning" style="border-radius: 10px !important;">
                <div class="card-header">
                    <h4  style="text-align:center; float:none !important;">Protocolos Covid-19 HSJD</h4>
                    <h5 class="card-title" style="text-align:center; float:none !important; font-size: 14px !important"> <a target="_blank" href="http://covidhsjd.sjdigital.cl/">Click aquí para acceder a todos los protocolos</a></h5> 
                </div>
            </div>
        </div>
        <div class="card" style="border-radius: 10px !important;">
            <div class="card-body login-card-body" style="border-radius: 10px !important;">
                <img src="{{url('/vendor/adminlte/dist/img/logo-sjd.png')}}" class="mx-auto d-block" style="width:200px; animation: pulse 2s infinite;" alt="">
                <p class="login-box-msg">{{ __('adminlte::adminlte.login_message') }}</p>
                <form action="{{ $login_url }}" method="post">
                    {{ csrf_field() }}
                    <div class="input-group mb-3">
                        <input type="text" name="rut" id="rut" class="form-control {{ $errors->has('rut') ? 'is-invalid' : '' }}" value="{{ old('rut') }}" placeholder="Rut sin puntos" pattern="[0-9]{7,8}-[0-9kK]{1}" autofocus>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user" style="color:#31BED7"></span>
                            </div>
                        </div>
                        @if ($errors->has('rut'))
                            <div class="invalid-feedback">
                                {{ $errors->first('rut') }}
                            </div>
                        @endif
                        <div class="invalid-feedback d-none" id="rutError">
                            Rut Invalido
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="{{ __('adminlte::adminlte.password') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock" style="color:#31BED7"></span>
                            </div>
                        </div>
                        @if ($errors->has('password'))
                            <div class="invalid-feedback">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" name="remember" id="remember">
                                <label for="remember">{{ __('adminlte::adminlte.remember_me') }}</label>
                            </div>
                        </div>
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block acceder" style="border-radius: 20px; font-weight: 600;">
                                {{ __('adminlte::adminlte.sign_in') }}
                            </button>
                        </div>
                    </div>
                </form>
                {{--
                    <p class="mt-2 mb-1">
                        <a href="mailto:derek.carvajal@redsalud.gov.cl?subject=No%20es%20Posible%20Inrgesar%20al%20Portal&body=Especifique%20su%20nombre%20y%20rut.">
                            No puedes ingresar?
                        </a>
                    </p>
                
                --}}
                @if ($register_url)
                    {{-- <p class="mb-0">
                        <a href="{{ $register_url }}">
                            {{ __('adminlte::adminlte.register_a_new_membership') }}
                        </a>
                    </p> --}}
                @endif
            </div>
        </div>
    </div>
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('js/jquery.rut.chileno.min.js') }}"></script>
    @stack('js')
    @yield('js')

    <script>
        $("#rut").change(function(){
            var value=$.rut.formatear($("#rut").val());
            $("#rut").val(value);
            if(!$.rut.validar(value)){
                $("#rutError").removeClass("d-none");
                $("#rutError").css("display", "inline");
            }
        });
    </script>
@stop
