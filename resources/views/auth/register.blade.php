@extends('adminlte::page')

@section('title', 'Portal')

@section('content_header')
@stop

@section('content')
    <div class=" col-md-12">
        <div class="register-logo">
            <img src="{{url('/vendor/adminlte/dist/img/logo-sjd.png')}}" class="mx-auto d-block" style="width:200px;" alt="">
        </div>
        <div class="card">
            <div class="card-body register-card-body">
            <p class="login-box-msg">{{ isset($edit) ? 'Editar cuenta' : 'Crear una nueva cuenta'}}</p>
            <form action="{{ url('/user') }}" method="post">
                {{ csrf_field() }}
                <div class="input-group mb-3">
                    <input type="text" required name="rut" id="rut" onblur="getPerson($(this).val());" {{ isset($edit) ? 'readonly' : ''}} class="form-control {{ $errors->has('rut') ? 'is-invalid' : '' }}" value="{{ old('rut') }}" pattern="[0-9]{7,8}-[0-9kK]{1}" onchange="verificarRut(this.value);" placeholder="Rut" autofocus>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-id-card"></span>
                        </div>
                    </div>
                    @if ($errors->has('rut'))
                        <div class="invalid-feedback">
                            <strong id="invalidRut">{{ $errors->first('rut') }}</strong>
                        </div>
                    @endif
                    <div class="invalid-feedback invalidRutDiv">
                        <strong id="invalidRut"></strong>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="text" name="nombre" id="nombre" class="form-control {{ $errors->has('nombre') ? 'is-invalid' : '' }}" value="{{ old('nombre') }}"
                           placeholder="Nombre Completo">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>

                    @if ($errors->has('nombre'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="input-group mb-3">
                    @if (Auth::user()->id_perfil == 1)
                        <select name="sistemas[]" multiple id="sistemas" class="form-control select2" onchange="selectSistemas($('#sistemas').val());">
                            <option></option>
                            @foreach($sistemas as $id => $sistema)
                                <option value="{{ $id }}" {{ (isset($event) && $event->sistema ? $event->sistema->id : old('venue_id')) == $id ? 'selected' : '' }}>{{ $sistema }}</option>
                            @endforeach
                        </select>
                    @else
                        <select name="sistemas[]" hidden multiple id="sistemas2" class="form-control ">
                            <option></option>
                            @foreach($sistemas as $id => $sistema)
                                <option  value="{{ $id }}" {{ (isset($event) && $event->sistema ? $event->sistema->id : old('venue_id')) == $id ? 'selected' : '' }}>{{ $sistema }}</option>
                            @endforeach
                        </select>
                    @endif
                    
                    @if ($errors->has('sistemas'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('sistemas') }}</strong>
                        </div>
                    @endif
                </div>
                @if (isset($edit))
                    <div class="input-group mb-3">
                        <input type="password" name="current_password" class="form-control {{ $errors->has('current_password') ? 'is-invalid' : '' }}" placeholder="{{ __('adminlte::adminlte.password') }} Actual">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        @if ($errors->has('current_password'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('current_password') }}</strong>
                            </div>
                        @endif
                    </div>
                @endif
                <div class="input-group mb-3">
                    <input type="password" {{ (isset($edit)) ? '' : 'required'}} name="password" id="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="{{ __('adminlte::adminlte.password') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @if ($errors->has('password'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="input-group mb-3">
                    <input type="password" {{ (isset($edit)) ? '' : 'required'}} name="password_confirmation" id="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" placeholder="{{ __('adminlte::adminlte.retype_password') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @if ($errors->has('password_confirmation'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </div>
                    @endif
                </div>
                @can('informatica', User::class)
                    <div class="input-group mb-3" id="userHospDiv">
                        <div class="form-check">
                            <input name="userHosp" id="userHosp" class="form-check-input" type="checkbox">
                            <label class="form-check-label">Crear en Hospitalizados</label>
                        </div>
                    </div>
                    <div id="divHosp" class="d-none">
                        <div class="input-group mb-3">
                            <select name="servicios[]" multiple id="servicios" class="form-control select2">
                                <option></option>
                            </select>
                        </div>
                        <div class="input-group mb-3" >
                            <select name="id_perfil" id="perfil" class="form-control select2">
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <br>
                @endcan
                @can('informatica', User::class)
                    <div class="input-group mb-3" id="userPabDiv">
                        <div class="form-check">
                            <input name="userPab" id="userPab" class="form-check-input" type="checkbox">
                            <label class="form-check-label">Crear en Pabellon</label>
                        </div>
                    </div>
                    <div id="divPab" class="d-none">
                        <div class="input-group mb-3">
                            <select name="perfilesPabellon[]" multiple id="perfilesPabellon" class="form-control select2" onchange="selectPerfilPab($('#perfilesPabellon').val());">
                                <option></option>
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <select name="pabellonesPabellon[]" multiple id="pabellonesPabellon" class="form-control select2" onchange="selectPabellonPab($('#pabellonesPabellon').val(), $('#bodegasPabellon').val());">
                                <option></option>
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <select name="bodegasPabellon[]" multiple id="bodegasPabellon" class="form-control select2">
                                <option></option>
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <select name="especialidadesPabellon[]" multiple id="especialidadesPabellon" class="form-control select2">
                                <option></option>
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <select name="equiposMedicoPabellon[]" multiple id="equiposMedicoPabellon" class="form-control select2">
                                <option></option>
                            </select>
                        </div>
                        <div class="input-group mb-3" id="equipoPabDiv">
                            <div class="form-check">
                                <input name="equipoPab" id="equipoPab" class="form-check-input" type="checkbox">
                                <label class="form-check-label">Crear en Equipo Medico</label>
                            </div>
                        </div>
                        <div id="divEquipoPab" class="d-none">
                            <div class="input-group mb-3" >
                                <select name="idTipoProfesionPabellon" id="tipoProfesionPabellon" class="form-control select2">
                                    <option></option>
                                </select>
                            </div>
                            <div class="input-group mb-3">
                                <select name="laboresPabellon[]" multiple id="laboresPabellon" class="form-control select2">
                                    <option></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="text" name="tx_nombre" id="tx_nombre" {{ isset($edit) ? 'readonly' : ''}} class="form-control" placeholder="Nombres" autofocus>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" name="tx_paterno" id="tx_paterno" {{ isset($edit) ? 'readonly' : ''}} class="form-control" placeholder="Apellido Paterno" autofocus>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" name="tx_materno" id="tx_materno" {{ isset($edit) ? 'readonly' : ''}} class="form-control" placeholder="Apellido Materno" autofocus>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                @endcan
                @can('informatica', User::class)
                    <div class="input-group mb-3" id="userComDiv">
                        <div class="form-check">
                            <input name="userCom" id="userCom" class="form-check-input" type="checkbox">
                            <label class="form-check-label">Crear en Comite Oncologico</label>
                        </div>
                    </div>
                    <div id="divCom" class="d-none">
                        <div class="input-group mb-3" >
                            <select name="id_especialidad" id="especialidad" class="form-control select2">
                                <option></option>
                            </select>
                        </div>
                        <div class="input-group mb-3" >
                            <select name="id_establecimiento" id="establecimiento" class="form-control select2">
                                <option></option>
                            </select>
                        </div>
                        <div class="input-group mb-3" >
                            <select name="id_tipoProfesional" id="tipoProfesional" class="form-control select2">
                                <option></option>
                            </select>
                        </div>
                        <div class="input-group mb-3" >
                            <select name="id_grupo" id="grupo" class="form-control select2">
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <br>
                @endcan
                @can('informatica', User::class)
                    <div class="input-group mb-3" id="userNotDiv">
                        <div class="form-check">
                            <input name="userNot" id="userNot" class="form-check-input" type="checkbox">
                            <label class="form-check-label">Crear en Notificacion de Eventos Adversos</label>
                        </div>
                    </div>
                    <div id="divNot" class="d-none">
                        <div class="input-group mb-3">
                            <input type="text" name="nombre_completo" id="nombre_completo" {{ isset($edit) ? 'readonly' : ''}} class="form-control" placeholder="Nombre Completo" autofocus>
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-id-card"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3" >
                            <select name="id_unidad" id="unidad" class="form-control select2">
                                <option></option>
                            </select>
                        </div>
                        <div class="input-group mb-3" >
                            <select name="id_estamento" id="estamento" class="form-control select2">
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <br>
                @endcan
                <button type="submit" class="btn btn-primary btn-block btn-flat">Guardar</button>
            </form>
            <p class="mt-2 mb-1">
                <a href="{{ url()->previous() }}"><i class="fa fa-arrow-alt-circle-left"></i> Volver</a>
            </p>
        </div>
        {{-- @dump(Auth::user()->rut) --}}
        <!-- /.form-box -->
    </div><!-- /.register-box -->
@stop

@section('adminlte_js')
    @include('sweet::alert')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script>
        var url = @json(url(''));
        $('#sistemas').select2({
            placeholder: "Seleccione Sistemas",
            width: "100%"
        });
        
        function verificarRut(rut) {
            $.getJSON(url+'/user/showByRut/'+rut, function(user) { 
                
                if (!jQuery.isEmptyObject(user)) {
                    var sistemas = user.sistemas.map(sistemas => sistemas['id']);
                    $('#nombre').val(user.nombre); 
                    $('#rut').val(user.rut); 
                    $('#invalidRut').html('Rut registrado anteriormente');
                    $("#sistemas").val(sistemas).trigger('change');
                    $("#sistemas2").val(sistemas).trigger('change');
                    $('.invalidRutDiv').addClass('valid-feedback d-block');
                    $('.invalidRutDiv').removeClass('invalid-feedback');
                    if (!jQuery.isEmptyObject(user.user_hospitalizado)){
                        $("#userHosp").css('display','').attr('checked', true);
                        var servicios = user.user_hospitalizado.servicios.map(servicios => servicios['id']);
                        verificarHospitalizado(servicios, user.user_hospitalizado.perfil_id, 1);                        
                    }
                    if (!jQuery.isEmptyObject(user.user_pabellon)){
                        $("#userPab").css('display','').attr('checked', true);
                        var perfiles = user.user_pabellon.perfiles.map(perfiles => perfiles['id']);
                        var bodegas = user.user_pabellon.bodegas.map(bodegas => bodegas['id']);
                        var especialidades = user.user_pabellon.especialidades.map(especialidades => especialidades['id']);
                        var equipos_medico = user.user_pabellon.equipos_medico.map(equipos_medico => equipos_medico['id']);
                        var pabellones = user.user_pabellon.pabellones.map(pabellones => pabellones['id']);
                        verificarPabellon(perfiles, bodegas, especialidades, equipos_medico, pabellones, 1);                        
                        if (!jQuery.isEmptyObject(user.user_pabellon.medico)){
                            $("#equipoPab").css('display','').attr('checked', true);
                            var labores = user.user_pabellon.medico.labores.map(labores => labores['cod_car']);
                            verificarMedicoPabellon(user.user_pabellon.medico.pro_nombres, user.user_pabellon.medico.pro_apepat, user.user_pabellon.medico.pro_apemat, labores, user.user_pabellon.medico.pro_tipo, 1);                            
                        }
                    }
                    if (!jQuery.isEmptyObject(user.user_comite)){
                        $("#userCom").css('display','').attr('checked', true);
                        if (user.user_comite.tx_tipo_profesional == 'STAFF'){
                            var tipoProfesional = 58;
                        }else{
                            var tipoProfesional = 59;
                        }
                        verificarComite(user.user_comite.tx_especialidad, user.user_comite.tx_establecimiento, tipoProfesional, user.user_comite.grupo.group_id);  
                    }
                    if (!jQuery.isEmptyObject(user.user_notificacion)){
                        $("#userNot").css('display','').attr('checked', true);
                        verificarNotificacion(user.user_notificacion.gl_fullname, user.user_notificacion.id_unidad, user.user_notificacion.id_estamento, 1);
                    }
                } else {
                    $('#invalidRut').html('Rut no registrado'); 
                    $('.invalidRutDiv').addClass('d-block');
                    $('#password').val(rut.substring(0,4));
                    $('#password_confirmation').val(rut.substring(0,4));
                }

            }).fail(function() {
                $('#invalidRut').html('Rut no registrado anteriormente'); 
                $('.invalidRutDiv').addClass('d-block');
            }); 
        };

        function getPerson(rut){
            $.getJSON("{{action('HomeController@getDatosRut')}}?rut="+rut,
            function(data){
                if(data.encontrado){
                    $("#rut").val(data.rutSinPuntos);
                    $("#nombre").val(data.nombreCompleto);
                    $("#tx_nombre").val(data.tx_nombre);
                    $("#tx_paterno").val(data.tx_apellido_paterno);
                    $("#tx_materno").val(data.tx_apellido_materno);
                    $("#nombre_completo").val(data.tx_apellido_paterno+' '+data.tx_apellido_materno+' '+data.tx_nombre);
                }
            })
        };
        
        @if (isset($edit))
            verificarRut(@json($edit));
        @endif

        $('#userHosp').change(function() {
            verificarHospitalizado(); 
        });

        function verificarHospitalizado(servicios, perfil, edit) {
            if(document.getElementById('userHosp').checked) {
                $.getJSON("{{action('HomeController@getParemetrosHosp')}}", function(data)
                {
                    $('#servicios').select2({
                        data: data.servicios,
                        placeholder: "Seleccione Servicios",
                        width: "100%"
                    });
                    
                    $('#perfil').select2({
                        data: data.perfiles,
                        placeholder: "Seleccione Perfil",
                        width: "100%"
                    });
                    if (edit == 1){
                        $("#servicios").val(servicios).trigger('change');
                        $("#perfil").val(perfil).trigger('change');
                    }
                });
                $('#divHosp').removeClass('d-none');    
            }else {
                $('#divHosp').addClass('d-none'); 
            }
        }
        
        $('#userPab').change(function() {
            verificarPabellon(); 
        });

        function verificarPabellon(perfiles, bodegas, especialidades, equipos_medico, pabellones, edit) {
            if(document.getElementById('userPab').checked) {
                $.getJSON("{{action('HomeController@getParemetrosPab')}}", function(data)
                {
                    $('#perfilesPabellon').select2({
                        data: data.perfiles,
                        placeholder: "Seleccione Perfiles",
                        width: "100%"
                    });

                    $('#bodegasPabellon').select2({
                        data: data.bodegas,
                        placeholder: "Seleccione Bodegas",
                        width: "100%"
                    });

                    $('#especialidadesPabellon').select2({
                        data: data.especialidades,
                        placeholder: "Seleccione Especialidades",
                        width: "100%"
                    });

                    $('#equiposMedicoPabellon').select2({
                        data: data.equiposMedico,
                        placeholder: "Seleccione Equipos Medicos",
                        width: "100%"
                    });

                    $('#pabellonesPabellon').select2({
                        data: data.pabellones,
                        placeholder: "Seleccione Pabellones",
                        width: "100%"
                    });
                    if (edit == 1){
                        $("#perfilesPabellon").val(perfiles).trigger('change');
                        $("#bodegasPabellon").val(bodegas).trigger('change');
                        $("#especialidadesPabellon").val(especialidades).trigger('change');
                        $("#equiposMedicoPabellon").val(equipos_medico).trigger('change');
                        $("#pabellonesPabellon").val(pabellones).trigger('change');
                    }
                });
                $('#divPab').removeClass('d-none');    
            }else {
                $('#divPab').addClass('d-none'); 
            }
        }

        $('#equipoPab').change(function() {
            verificarMedicoPabellon();    
        });

        function verificarMedicoPabellon(nombre, paterno, materno, labores, tipoProfesional, edit) {
            if(document.getElementById('equipoPab').checked) {
                $.getJSON("{{action('HomeController@getParemetrosEquipoPab')}}", function(data)
                {
                    $('#tipoProfesionPabellon').select2({
                        data: data.profesiones,
                        placeholder: "Seleccione Profesion",
                        width: "100%"
                    });

                    $('#laboresPabellon').select2({
                        data: data.labores,
                        placeholder: "Seleccione Labores",
                        width: "100%"
                    });
                    if (edit == 1){
                        $("#tx_nombre").val(nombre);
                        $("#tx_paterno").val(paterno);
                        $("#tx_materno").val(materno);
                        $("#laboresPabellon").val(labores).trigger('change');
                        $("#tipoProfesionPabellon").val(tipoProfesional).trigger('change');
                    }
                });
                $('#divEquipoPab').removeClass('d-none');   
            }else {
                $('#divEquipoPab').addClass('d-none'); 
            }
        }

        $('#userCom').change(function() {
            verificarComite();  
        });

        function verificarComite(especialidad, establecimiento, tipoProfesional, grupo, edit) {
            if(document.getElementById('userCom').checked) {
                $.getJSON("{{action('HomeController@getParemetrosCom')}}", function(data)
                {
                    $('#especialidad').select2({
                        data: data.especialidades,
                        placeholder: "Seleccione Especialidad",
                        width: "100%"
                    });
                    $('#establecimiento').select2({
                        data: data.establecimientos,
                        placeholder: "Seleccione Establecimiento",
                        width: "100%"
                    });
                    $('#tipoProfesional').select2({
                        data: data.tiposProfesional,
                        placeholder: "Seleccione Tipo Profesional",
                        width: "100%"
                    });
                    $('#grupo').select2({
                        data: data.grupos,
                        placeholder: "Seleccione Grupo",
                        width: "100%"
                    });
                    $("#especialidad").val(especialidad).trigger('change');
                    $("#establecimiento").val(establecimiento).trigger('change');
                    $("#tipoProfesional").val(tipoProfesional).trigger('change');
                    $("#grupo").val(grupo).trigger('change');
                });
                $("#especialidad").css('display','').attr('required', true);
                $("#establecimiento").css('display','').attr('required', true);
                $("#tipoProfesional").css('display','').attr('required', true);
                $("#grupo").css('display','').attr('required', true);
                $('#divCom').removeClass('d-none');    
            }else {
                $("#especialidad").css('display','').attr('required', false);
                $("#establecimiento").css('display','').attr('required', false);
                $("#tipoProfesional").css('display','').attr('required', false);
                $("#grupo").css('display','').attr('required', false);
                $('#divCom').addClass('d-none'); 
            }
        }

        $('#userNot').change(function() {
            verificarNotificacion();  
        });

        function verificarNotificacion(nombre, unidad, estamento, edit) {
            if(document.getElementById('userNot').checked) {
                $.getJSON("{{action('HomeController@getParemetrosNot')}}", function(data)
                {
                    $('#unidad').select2({
                        data: data.unidades,
                        placeholder: "Seleccione Unidad",
                        width: "100%"
                    });
                    $('#estamento').select2({
                        data: data.estamentos,
                        placeholder: "Seleccione Estamento",
                        width: "100%"
                    });
                    if (edit == 1){
                        $("#nombre_completo").val(nombre);
                        $("#unidad").val(unidad).trigger('change');
                        $("#estamento").val(estamento).trigger('change');
                    }
                });
                $("#unidad").css('display','').attr('required', true);
                $("#estamento").css('display','').attr('required', true);
                $('#divNot').removeClass('d-none');  
            }else {
                $("#unidad").css('display','').attr('required', false);
                $("#estamento").css('display','').attr('required', false);
                $('#divNot').addClass('d-none'); 
            }
        }

        function selectSistemas(sistemas){
            if(sistemas.includes('6') || sistemas.includes('4') || sistemas.includes('9') || sistemas.includes('14')){
                document.getElementById("userHosp").checked = true;
            }else{
                document.getElementById("userHosp").checked = false;
            }
            verificarHospitalizado();
            if(sistemas.includes('7')){
                document.getElementById("userPab").checked = true;
            }else{
                document.getElementById("userPab").checked = false;
            }
            verificarPabellon();
            if(sistemas.includes('2')){
                document.getElementById("userCom").checked = true;
            }else{
                document.getElementById("userCom").checked = false;
            }
            verificarComite();
            if(sistemas.includes('5')){
                document.getElementById("userNot").checked = true;
            }else{
                document.getElementById("userNot").checked = false;
            }
            verificarNotificacion();
        }

        function selectPerfilPab(perfiles){
            // console.log(perfiles);
            // verificarMedicoPabellon();
            // if(perfiles.includes('4') || perfiles.includes('24')){
            //     document.getElementById("equipoPab").checked = true;
            //     if(perfiles.includes('24')){
            //         // $("#laboresPabellon").val(labores).trigger('change');
            //         // x = 'E';
            //         // console.log(x);
            //         $("#tipoProfesionPabellon").val('E').trigger('change');
            //     }
            // }else{
            //     document.getElementById("equipoPab").checked = false;
            // }
            // verificarMedicoPabellon();
            // // x = 'E';
            // $("#tipoProfesionPabellon").val('E').trigger('change');
        }

        function selectPabellonPab(pabellones, bodegas){
            if(pabellones.includes('1') && !bodegas.includes('5')){
                var newState = new Option('Bodega General 6° Piso', 5, true, true);
                $("#bodegasPabellon").append(newState).trigger('change');
            }
            if(pabellones.includes('2') && !bodegas.includes('1')){
                var newState = new Option('Bodega General 5° Piso', 1, true, true);
                $("#bodegasPabellon").append(newState).trigger('change');
            }
            if(pabellones.includes('3') && !bodegas.includes('6')){
                var newState = new Option('Bodega General UCA', 6, true, true);
                $("#bodegasPabellon").append(newState).trigger('change');
            }
        }
    </script>
    @stack('js')
    @yield('js')
@stop
