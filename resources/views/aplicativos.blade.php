@extends('adminlte::pageAplicativos')

@section('title', 'Aplicativos HSJD')
@section('logo', 'Portal Aplicativos')
@section('logo_img', 'Portal Aplicativos')

@section('content_header')
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Bienvenido(a),<b style="color:rgb(54, 175, 195)"> </b> <small><b>IP:</b> {{ Request::ip() }}</small></h3> 
        <div class="card-tools">
            <div class="form-inline ml-3">

                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" id="search" type="search" placeholder="Buscar" aria-label="Search">
                    <div class="input-group-append">
                      <button class="btn btn-navbar" type="submit">
                        <i class="fa fa-search"></i>
                      </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</div>

<div class="card card-info card-tabs">
    <div class="card-header p-0 pt-1">
      <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
        <li class="nav-item">
          <a class="nav-link {{ empty(request()->query()) ? 'active' : ''}}" id="ministeriales" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="{{ empty(request()->query()) ? 'true' : 'false'}}">Ministeriales</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="ficha_electrónica" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Ficha Electrónica</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="servicios_administrativos" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">Servicios Administrativos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="utilitarios" data-toggle="pill" href="#custom-tabs-one-settings" role="tab" aria-controls="custom-tabs-one-settings" aria-selected="false">Utilitarios</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="otros_establecimientos" data-toggle="pill" href="#custom-tabs-one-otrosestablecimientos" role="tab" aria-controls="custom-tabs-one-settings" aria-selected="false">Otros Establecimientos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="informatica" data-toggle="pill" href="#custom-tabs-one-informatica" role="tab" aria-controls="custom-tabs-one-settings" aria-selected="false">Informática</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ !empty(request()->query()) ? 'active' : ''}}" id="anexos" data-toggle="pill" href="#custom-tabs-one-anexos" role="tab" aria-controls="custom-tabs-one-anexos" aria-selected="{{ !empty(request()->query()) ? 'true' : 'false'}}">Anexos</a>
        </li>
      </ul>
    </div>
    <div class="card-body">
      <div class="tab-content" id="custom-tabs-one-tabContent">
        {{-- ministeriales, 1 --}}
        <div class="tab-pane fade {{ empty(request()->query()) ? 'active show' : ''}}" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="ministeriales">
            @include('partials.aplicativosCard', ['tipo'=> 1])
        </div>
        {{-- ficha electroinca, 2 --}}
        <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="ficha_electrónica">
            @include('partials.aplicativosCard', ['tipo'=> 2])
        </div>
        {{-- servicios administrativos, 4 --}}
        <div class="tab-pane fade" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="servicios_administrativos">
            @include('partials.aplicativosCard', ['tipo'=> 4])
        </div>
        {{-- utilitarios,  5--}}
        <div class="tab-pane fade" id="custom-tabs-one-settings" role="tabpanel" aria-labelledby="utilitarios">
            @include('partials.aplicativosCard', ['tipo'=> 5])
        </div>
        {{-- otros establecimisentos,  no esta en la BD--}}
        <div class="tab-pane fade" id="custom-tabs-one-otrosestablecimientos" role="tabpanel" aria-labelledby="otros_establecimientos">
            @include('partials.aplicativosCard', ['tipo'=> 4])
        </div>
        {{-- informatica,  7--}}
        <div class="tab-pane fade" id="custom-tabs-one-informatica" role="tabpanel" aria-labelledby="informatica">
            @include('partials.aplicativosCard', ['tipo'=> 7])
        </div>
        {{-- anexos,  --}}
        <div class="tab-pane fade {{ !empty(request()->query()) ? 'active show' : ''}}" id="custom-tabs-one-anexos" role="tabpanel" aria-labelledby="anexos">
            @include('partials.anexosCard', ['tipo'=> 7])
        </div>
      </div>
    </div>
    <!-- /.card -->
  </div>
@include('partials.contactosModal')
@stop

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
@stop

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
@include('sweet::alert')
<script> 
	$('.brand-image').removeClass('elevation-3'); 
	$('.brand-image').removeClass('img-circle'); 
    $('.brand-text').css('color', '#10879a'); 
    $('[data-toggle="tooltip"]').tooltip();
    
    $( "#search" ).change(function() {
        if ($( "#search" ).val() != '') {
            var search = $( "#search" ).val();
            $('.aplicativo-card').css('display', 'none'); 
            $("*[id*="+search+"]").each(function() {
                $(this).css('display', '');
            });
        }else {
            $('.aplicativo-card').css('display', ''); 
        }
    });
   
</script>
@stop