<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="modalSoporte" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Contactos Soporte Sistemas Locales</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            {{-- <h3>
                <strong>Nuevo:</strong> 
                <small>Asistencia vía sistema de Tickets</small>
                <a class="btn btn-primary btn-sm" target="_blank" href="http://10.4.237.33/ticket/public/create-ticket">Ingresar Aquí</a>
            </h3>
            <br> --}}
            <table class="table table-hover">
                @foreach ($soporte as $user)
                    <tr>
                        <td><b>{{$user->nombre}}</b></td>
                        <td><a href="mailto:{{$user->mail}}"><i class="fa fa-envelope"></i></a></td>
                        <td>{{$user->mail}}</td>
                        <td style="width:20%">{{$user->sistema}}</td>
                        <td><i class="fa fa-phone-square-alt"></i> {{$user->telefono}}</td>
                    </tr>
                @endforeach
            </table>
            
        </div>
      </div>
    </div>
</div><!-- /.modal -->