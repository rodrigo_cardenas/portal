<div class="row">
   {{-- filtros --}}
   <form class="form-horizontal">
        <div class="form-group row">
            <div class="col-sm-4">
            <input type="text" name="nombre" class="form-control" value="{{ request()->nombre }}" id="inputEmail3" placeholder="Nombre">
            </div>
            <div class="col-sm-3">
                <input type="text" name="depto" class="form-control" value="{{ request()->depto }}" id="inputEmail3" placeholder="Depto">
            </div>
            <div class="col-sm-4">
                <input type="text" name="establecimiento" class="form-control" value="{{ request()->establecimiento }}" id="inputEmail3" placeholder="Establecimiento">
            </div>
            <div class="col-sm-1">
                <button type="submit" class="btn btn-info">Filtrar</button>
            </div>
        </div>
    </form>
    {{-- tabla usuarios --}}
    <table class="table table-striped table-hover" id="tableUsers">
        <thead>
            <th>Anexo</th>
            <th>Nombre</th>
            <th>Depto</th>
            <th>Piso</th>
            <th>Establecimiento</th>
        </thead>
        <tbody>
            @foreach ($anexos as $anexo)
            <tr>
                <td>{{ $anexo->anexo }}</td>
                <td>{{ $anexo->nombre }} {{ $anexo->apepat }} {{ $anexo->apemat }}</td>
                <td>{{ $anexo->depto }}</td>
                <td>{{ $anexo->piso }}</td>
                <td>{{ $anexo->dir }}</td>
                
            </tr>
            @endforeach
        </tbody>
    </table>
    
    {{ $anexos->appends(request()->query())->fragment('custom-tabs-one-anexos')->links() }}
</div>