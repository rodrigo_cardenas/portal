<div class="row">
    @forelse ($aplicativos->where('id_categoria', $tipo) as $aplicativo)
        <div class="col-md-3 aplicativo-card" id="{{ $aplicativo->gl_titulo}}">
            <div class="card card-widget widget-user" style="min-height:300px">
                {{-- <div class="widget-user-header text-white" style="background: url('{{ $aplicativo->gl_img}}')  center center;">
                    <h5 class="widget-user-username text-center  badge-info" style="font-size: medium; border-radius: 7px !important;"><b> {{ $aplicativo->gl_titulo}}</b></h5>
                </div> --}}
                <div class="card card-widget widget-user">
                    <img src="{{ asset("img/aplicativos/{$aplicativo->gl_img}") }}" alt="" width="100%" height="150" style="max-height: 150px; object-fit; fill" loading='lazy'>
                    <h5 class="widget-user-username text-center  badge-info" style="font-size: medium; border-radius: 7px !important; position: absolute; margin-top: 25px; width: 100%"><b> {{ $aplicativo->gl_titulo}}</b></h5>
                </div>
                <div class="card-footer" style="padding-top: 10px; background-color:white">
                    <div class="caption text-center">
                        <p class="text-center" style="font-size: small;">{{ $aplicativo->gl_descripcion}}</p>
                        <p class="text-center margin-0" style="font-size: 12px;"><strong>Usuario :</strong> {{ $aplicativo->gl_cliente}}</p>
                        <a href="{{ $aplicativo->gl_url}}" target="_blank" class="btn btn-outline-info btn-sm " style="position: inline; margin-right: -10px; ">Ingresar</a>
                    </div>
                </div>
            </div>
        </div>
    @empty
            
    @endforelse
</div>