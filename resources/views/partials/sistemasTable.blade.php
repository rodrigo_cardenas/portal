<table id="example2" class="table table-hover compact ">
    <thead class="d-none">
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @forelse ($sistemas as $sistema)
            @php
                $urlFav = ($sistema->pivot->favorito) ? url('/user/editFav/0/'.$sistema->id) : url('/user/editFav/1/'.$sistema->id);
                $urlDeleteSistema = url('/user/deleteUserSistema/'.$sistema->id);
                $urlSistema = str_replace('$rUser', Auth::user()->rut, $sistema->tx_direccion);
                $urlSistema = str_replace('$tokenPortal', Auth::user()->userPortal->token ?? '', $urlSistema);
            @endphp
            <tr>
                <td>
                    <a href="{{ $urlSistema }}" target="_blank"><img class="rounded-circle activo" src="{{url('/img/'.$sistema->tx_imagen)}}" style="border-radius: 50%!important;" width="30" height="30" id="img_6"></a>
                </td>
                <td>
                    <h3 style="padding: 0px; margin: 0px !important"><a href="{{ $urlSistema }}" style="color:#447AC2" target="_blank">{{ $sistema->tx_descripcion }}</a> <i style="color:gold" class="fa fa-xs fa-star" {{ ($sistema->pivot->favorito) ? '' : 'hidden'}}></i></h3>
                </td>
                <td>
                    <a class="btn btn-primary btn-sm activo" href="{{ $urlSistema }}" role="button" target="_blank" id="btn_3">Ingresar »</a>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle dropdown-icon btn-sm" data-toggle="dropdown" >
                        <div class="dropdown-menu" >
                            <a class="dropdown-item" onclick="location.href='{{ $urlFav }}'">
                                {{ ($sistema->pivot->favorito) ? 'Quitar de Favoritos' : 'Agregar a Favoritos'}}
                            </a>
                            @can('informatica', User::class)
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" onclick="confirmar('{{$urlDeleteSistema}}');">Eliminar Sistema</a>
                            @endcan
                        </div>
                        </button>
                    </div>
                </td>
                <td>
                    <i style="margin-top:10px;" class="fa fa-lg fa-info-circle" data-toggle="tooltip" data-placement="top" title="{{ $sistema->tx_info }}"></i>
                </td>
            </tr>
        @empty
            
        @endforelse
    </tbody>
</table>