<div class="info-box bg-info">
    <span class="info-box-icon"><i class="fa fa-info-circle"></i></span>
    <div class="info-box-content">
        <span class="info-box-text"><b>Nuevo:</b> Asistencia vía sistema de Tickets </span>
        <div class="progress">
            <div class="progress-bar" style="width: 100%"></div>
        </div>
        <p class="">
            <em>Estimadas/os Usuarios: Se ha implementado un sistema de Tickets para agilizar la comunicacion entre la unidad de desarrollo y los usuarios de HSJDigital.</em>
        </p>
        
        <p class="">
            <em>Para esto no debe tener cuenta de usuario, solo ingrese al siguiente enlace y llene el formulario con los datos del incidente. </em>
        </p>
        <a class="btn btn-primary btn-sm" target="_blank" href="http://10.4.237.33/ticket/public/create-ticket">Ingresar Ticket</a>
    </div>
</div>