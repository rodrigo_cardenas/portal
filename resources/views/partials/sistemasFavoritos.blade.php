<div class="row">
    @forelse ($sistemas as $sistema)
        @if ($sistema->pivot->favorito)
            @php
                $urlFav = ($sistema->pivot->favorito) ? url('/user/editFav/0/'.$sistema->id) : url('/user/editFav/1/'.$sistema->id);
                $urlDeleteSistema = url('/user/deleteUserSistema/'.$sistema->id);
                $urlSistema = str_replace('$rUser', Auth::user()->rut, $sistema->tx_direccion);
                $urlSistema = str_replace('$tokenPortal', Auth::user()->userPortal->token ?? '', $urlSistema);
            @endphp
            <div class="col-sm-3">
                <div style="width: 15rem;" class="card">
                    <a href="{{ $urlSistema }}" target="_blank">
                        <img alt="Card image cap" src="{{url('/img/'.$sistema->tx_imagen)}}" style="border-top-left-radius: 5px;border-top-right-radius: 5px;" class="card-img-top">
                    </a>
                    <i class="fa fa-xs fa-star" style="color:gold; font-size:20px; position: absolute; top:10px;right: 10px;"></i>
                    <div class="card-body">
                        <div class="row">

                            <div class="col-md-9">
                                <span class="card-title" style="">{{ $sistema->tx_descripcion }} </span>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ $urlSistema }}" target="_blank" class="btn btn-outline-info btn-sm float-right" style="position: inline; margin-right: -10px; ">Ingresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endif
    @empty
            
    @endforelse
</div>