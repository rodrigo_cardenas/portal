<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParametroNotificacion extends Model
{
    protected $connection = 'mysql5';
    protected $table = 'cl_parametro';
}
