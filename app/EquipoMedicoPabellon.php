<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipoMedicoPabellon extends Model
{
    protected $connection = 'mysql3';
    protected $table = 'cb_equipo_medico';
    public $guarded = [];
}
