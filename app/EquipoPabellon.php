<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipoPabellon extends Model
{
    protected $connection = 'mysql3';
    protected $table = 'cb_medico_ncr';
    protected $primaryKey = 'pro_cod';
    public $guarded = [];

    public function TipoProfesion()
    {
        return $this->hasOne('App\TipoProfesionPabellon', 'pro_tipo', 'id');
    }

    public function Labores()
    {
        return $this->belongsToMany('App\LaborPabellon', 'cb_medico_cod_ncr', 'pro_cod', 'tla_cod', 'pro_cod', 'cod_car');
    }
}
