<?php

namespace App;

// namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserPortal extends Authenticatable
{
    protected $guard = 'userPortal';

    protected $connection = 'mysql9';
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'nombre', 'rut', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'token',
    ];

    public function sistemas()
    {
        return $this->belongsToMany('App\Sistemas', 'gen_user_sistemas', 'id_user', 'id_sistema')->withPivot('favorito');
    }
}
