<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHospitalizados extends Model
{
    // protected $connection = 'mysql3';
    protected $table = 'hospitalizacion.users';
    public $guarded = [];
    

    public function setNombreAttribute($value){
        $this->attributes['nombre'] = ucfirst($value);
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = bcrypt(intval($value));
    }

    public function servicios()
    {
        return $this->belongsToMany('App\Servicio', 'hospitalizacion.hos_usuario_servicio', 'id_usuario', 'id_servicio');
    }
    
    // public function userServicios()
    // {
    //     return $this->hasMany('App\UserServicios', 'id_usuario', 'id_servicio');
    // }
}
