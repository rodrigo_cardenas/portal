<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'string|required',
            'rut' => 'string|required',
            'password' => 'confirmed',
            'sistemas' => 'array|nullable'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        // checks user current password
        if (!empty($this->current_password)) {
            $validator->after(function ($validator) {
                if ( !Hash::check($this->current_password, $this->user()->password) ) {
                    $validator->errors()->add('current_password', 'Contraseña actual incorrecta.');
                }
            });
        }
        return;
    }
}
