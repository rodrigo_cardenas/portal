<?php

namespace App\Http\Controllers\Auth;

use App\UserPortal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserPortalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:userPortal');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = UserPortal::find('3367');
        $sistemas = $usuario->sistemas()->orderBy('favorito', 'desc')->get();
        return view('home', compact('usuario', 'sistemas'));
        return view('home');
    }
}
