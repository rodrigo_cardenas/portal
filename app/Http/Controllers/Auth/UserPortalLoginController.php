<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Route;

class UserPortalLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:userPortal', ['except' => ['logout']]);
    }
    
    public function showLoginForm()
    {
      return view('auth.login');
    }
    
    public function login(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'rut'   => 'required|string',
        'password' => 'required|min:4'
      ]);
      
      // Attempt to log the user in
      if (Auth::guard('userPortal')->attempt(['rut' => $request->rut, 'password' => $request->password], $request->remember)) {
        // dd($request->all());
        // if successful, then redirect to their intended location
        return redirect()->intended(route('home'));
      } 
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('rut', 'remember'));
    }
    
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin');
    }
}
