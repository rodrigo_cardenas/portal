<?php

namespace App\Http\Controllers;

use App\User;
use App\Anexos;
use App\Perfil;
use App\Servicio;
use App\UserPortal;
use App\Aplicativos;
use App\Informaciones;
use App\PerfilPabellon;
use App\BodegaPabellon;
use App\EspecialidadPabellon;
use App\EquipoMedicoPabellon;
use App\PabellonPabellon;
use App\TipoProfesionPabellon;
use App\LaborPabellon;
use App\EspecialidadComite;
use App\EstablecimientoComite;
use App\TipoProfesionalComite;
use App\GrupoComite;
use App\ParametroNotificacion;
use App\SoporteSistemas;
use App\Api\FonasaApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('resetPass', 'aplicativos');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sistemas = Auth::user()->sistemas()->orderBy('tx_descripcion', 'ASC')->orderBy('favorito', 'desc')->get();
        $informaciones = Informaciones::latest()->first() ?? null;
        $soporte = SoporteSistemas::get();
        // dd($soporte);
        return view('home', compact('sistemas', 'informaciones', 'soporte'));
    }

    /**
     * Show the aplicativos dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function aplicativos(Request $request)
    {
        $aplicativos = Aplicativos::orderBy('gl_titulo')->get();
        $anexos = Anexos::orderBy('depto')
            ->when($request->has('nombre') && !is_null($request->nombre), function ($collection) use ($request) {
                return $collection->where('nombre', 'like', '%'.$request->nombre.'%');
            })    
            ->when($request->has('depto') && !is_null($request->depto), function ($collection) use ($request) {
                return $collection->where('depto', 'like', '%'.$request->depto.'%');
            })    
            ->when($request->has('establecimiento') && !is_null($request->establecimiento), function ($collection) use ($request) {
                return $collection->where('dir', 'like', '%'.$request->establecimiento.'%');
            })    
            ->paginate(10);
        return view('aplicativos', compact('anexos', 'aplicativos'));
    }

    public function resetPass()
    {
        $users = User::where('rut', '!=', '17534033-5')->get();
        $users->each(function ($user) {
            $user->update(['password' => intval(substr($user->rut, 0, 4))]);
        });
    }
    
    public function getParemetrosHosp()
    {
        $paremetros['servicios'] = Servicio::select('id', 'tx_descripcion as text')->where('cd_tipo_atencion', 'H')->orWhereIn('id', [9, 13, 3, 7, 19, 14, 12, 20, 21])->toBase()->get();
        $paremetros['perfiles'] = Perfil::select('id', 'tx_descripcion as text')->toBase()->get();

        return response()->json($paremetros);
    }

    public function getParemetrosPab()
    {
        $paremetros['perfiles'] = PerfilPabellon::select('id', 'gl_nombre as text')->toBase()->get();
        $paremetros['bodegas'] = BodegaPabellon::select('id', 'gl_nombre as text')->toBase()->get();
        $paremetros['especialidades'] = EspecialidadPabellon::select('id', 'gl_nombre as text')->toBase()->get();
        $paremetros['equiposMedico'] = EquipoMedicoPabellon::select('id', 'gl_descripcion as text')->toBase()->get();
        $paremetros['pabellones'] = PabellonPabellon::select('id', 'gl_seudonimo as text')->toBase()->get();

        return response()->json($paremetros);
    }

    public function getParemetrosEquipoPab()
    {
        $paremetros['profesiones'] = TipoProfesionPabellon::select('id', 'gl_nombre as text')->toBase()->get();
        $paremetros['labores'] = LaborPabellon::select('cod_car as id', 'cod_des as text')->toBase()->get();

        return response()->json($paremetros);
    }

    public function getParemetrosCom()
    {
        $paremetros['especialidades'] = EspecialidadComite::select('id', 'tx_nombre as text')->toBase()->get();
        $paremetros['establecimientos'] = EstablecimientoComite::select('id', 'tx_nombre as text')->toBase()->get();
        $paremetros['tiposProfesional'] = TipoProfesionalComite::select('id', 'tx_nombre as text')->toBase()->get();
        $paremetros['grupos'] = GrupoComite::select('id', 'tx_nombre as text')->toBase()->get();

        return response()->json($paremetros);
    }

    public function getParemetrosNot()
    {
        $paremetros['unidades'] = ParametroNotificacion::where('gl_tipo', 'Unidad')->select('id', 'gl_valor as text')->toBase()->get();
        $paremetros['estamentos'] = ParametroNotificacion::where('gl_tipo', 'Estamentos')->select('id', 'gl_valor as text')->toBase()->get();

        return response()->json($paremetros);
    }

    public function getDatosRut(Request $request, FonasaApi $fonasaApi)
    {
        $rut = str_replace(".","",request()->rut);
        $rutDV = explode("-", $rut);
        $data = $fonasaApi->fetchNormalized($rutDV[0], $rutDV[1]);
        $data['rutSinPuntos'] = strtoupper($rut);
        $data['nombreCompleto'] = $data['tx_nombre']." ".$data['tx_apellido_paterno']." ".$data['tx_apellido_materno'];
        return $data;
    }
}
