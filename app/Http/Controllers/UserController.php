<?php

namespace App\Http\Controllers;

use App\User;
use App\Perfil;
use App\Servicio;
use App\Sistemas;
use App\UserHospitalizados;
use App\UserPabellon;
use App\EquipoPabellon;
use App\UserComite;
use App\UserGrupoComite;
use App\UserNotificacion;
use App\ParametroNotificacion;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUser;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->authorizeResource(User::class, 'user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->rut)){
            $rut = str_replace(".","",request()->rut);
        }else{
            $rut = null;
        }
        $users = User::with('sistemas')->latest('id')
                ->when($request->has('rut') && !is_null($request->rut), function ($collection) use ($rut) {
                    return $collection->whereRaw('rut LIKE ?', '%'.$rut.'%');
                })
                ->when($request->has('nombre') && !is_null($request->nombre), function ($collection) use ($request) {
                    return $collection->where('nombre', 'like', '%'.$request->nombre.'%');
                })
                ->paginate(15);
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sistemas = Sistemas::all()->pluck('tx_descripcion', 'id');
        return view('auth.register', compact('sistemas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        $dataRequest = (empty($request->password)) ? $request->only('nombre', 'rut') : $request->only('nombre', 'rut', 'password');
        
        $user = User::withTrashed()->updateOrCreate(['rut'=> $request->rut], array_merge($dataRequest, ['deleted_at' => null]));
        $user->sistemas()->sync($request->sistemas);

        $userHospitalizados = ($request->userHosp == 'on') ? $this->insertUserHospitalizados($request) : null;
        $userPabellon = ($request->userPab == 'on') ? $this->insertUserPabellon($request) : null;
        $equipoPabellon = ($request->userPab == 'on' && $request->equipoPab == 'on') ? $this->insertEquipoPabellon($request) : null;
        $userComite = ($request->userCom == 'on') ? $this->insertUserComite($request) : null;
        $userNotificacion = ($request->userNot == 'on') ? $this->insertUserNotificacion($request) : null;
 
        if (!$user) {
            return redirect()->back()->with('error', 'Error al guardar');
        }
        return redirect('/home')->with('success', 'Registro Guardado Correctamente!');
    }
    
    /**
     * Store a newly created resource in users BD:Hospitalizacion.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertUserHospitalizados(Request $request)
    {
        $dataRequest = [
            'name' => $request->nombre,
            'rut' => $request->rut,
            'password' => $request->password,
            'perfil_id' => $request->id_perfil,
            'servicio_id' => $request->servicios[0],
        ];

        $user = UserHospitalizados::updateOrCreate(['rut'=> $request->rut], $dataRequest);
        $user->servicios()->sync($request->servicios);
 
        if (!$user) {
            return redirect()->back()->with('error', 'Error al guardar');
        }
        return $user;
    }

    public function insertUserPabellon(Request $request)
    {
        $dataRequest = [
            'gl_nombre' => $request->nombre,
            'gl_rut' => $request->rut,
            'gl_clave' => $request->password,
        ];

        $user = UserPabellon::updateOrCreate(['gl_rut'=> $request->rut], $dataRequest);
        $user->perfiles()->sync($request->perfilesPabellon);
        $user->bodegas()->sync($request->bodegasPabellon);
        $user->especialidades()->sync($request->especialidadesPabellon);
        $user->equiposMedico()->sync($request->equiposMedicoPabellon);
        $user->pabellones()->sync($request->pabellonesPabellon);
 
        if (!$user) {
            return redirect()->back()->with('error', 'Error al guardar');
        }
        return $user;
    }

    public function insertEquipoPabellon(Request $request)
    {
        $rut = explode("-", $request->rut);

        $dataRequest = [
            'pro_nombres' => $request->tx_nombre,
            'pro_apepat' => $request->tx_paterno,
            'pro_apemat' => $request->tx_materno,
            'pro_rut' => $rut[0],
            'pro_digito' => $rut[1],
            'pro_tipo' => $request->idTipoProfesionPabellon,
        ];

        $user = EquipoPabellon::updateOrCreate(['pro_rut'=> $rut[0]], $dataRequest);
        $user->labores()->sync($request->laboresPabellon);
 
        if (!$user) {
            return redirect()->back()->with('error', 'Error al guardar');
        }
        return $user;
    }

    public function insertUserComite(Request $request)
    {
        if($request->id_tipoProfesional == 58){
            $tipoProfesional = "STAFF";
        }else{
            $tipoProfesional = "BECADO";
        }
        $dataRequest = [
            'name' => $request->nombre,
            'login' => $request->rut,
            'password' => $request->password,
            'pswd' => $request->password,
            'active' => 'Y',
            'tx_especialidad' => $request->id_especialidad,
            'tx_establecimiento' => $request->id_establecimiento,
            'tx_tipo_profesional' => $tipoProfesional,
        ];

        $user = UserComite::updateOrCreate(['login'=> $request->rut], $dataRequest);
        $user = UserGrupoComite::updateOrCreate([
            'login'=> $request->rut
        ],[
            'login'=> $request->rut,
            'group_id'=> $request->id_grupo
        ]);
 
        if (!$user) {
            return redirect()->back()->with('error', 'Error al guardar');
        }
        return $user;
    }

    public function insertUserNotificacion(Request $request)
    {
        $rut = explode("-", $request->rut);
        $gl_unidad = ParametroNotificacion::where('gl_tipo', 'Unidad')->where('id', $request->id_unidad)->pluck('gl_valor')->first();
        $gl_estamento = ParametroNotificacion::where('gl_tipo', 'Estamentos')->where('id', $request->id_estamento)->pluck('gl_valor')->first();
        $dataRequest = [
            'gl_user' => $request->rut,
            'gl_pass' => $rut[0],
            'gl_fullname' => $request->nombre_completo,
            'gl_unidad' => $gl_unidad,
            'gl_estamento' => $gl_estamento,
            'gl_perfil' => 2,
        ];

        $user = UserNotificacion::updateOrCreate(['gl_user'=> $request->rut], $dataRequest);
 
        if (!$user) {
            return redirect()->back()->with('error', 'Error al guardar');
        }
        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Display the specified resource by rut.
     *
     * @param  int  $rut
     * @return \Illuminate\Http\Response
     */
    public function showByRut($rut)
    {
        $user = User::with('sistemas', 'userHospitalizado', 'userHospitalizado.servicios', 'userPabellon', 'userPabellon.perfiles', 'userPabellon.bodegas', 'userPabellon.especialidades', 'userPabellon.equiposMedico', 'userPabellon.pabellones', 'userComite', 'userComite.grupo', 'userNotificacion')->whereRut($rut)->first() ?? new User;
        if(isset($user->id) && isset($user->userNotificacion)){
            $user->userNotificacion->id_unidad = ParametroNotificacion::where('gl_tipo', 'Unidad')->where('gl_valor', $user->userNotificacion->gl_unidad)->pluck('id')->first();
            $user->userNotificacion->id_estamento = ParametroNotificacion::where('gl_tipo', 'Estamentos')->where('gl_valor', $user->userNotificacion->gl_estamento)->pluck('id')->first();
        }
        return $user->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $sistemas = Sistemas::all()->pluck('tx_descripcion', 'id');
        $edit = $user->rut;
        $user = User::with('userHospitalizado', 'userPabellon', 'userComite')->find($user->id);
        return view('auth.register', compact('user', 'medico', 'sistemas', 'edit'));
    }
    
    public function editMe()
    {
        $sistemas = Sistemas::all()->pluck('tx_descripcion', 'id');
        $edit = Auth::user()->rut;
        return view('auth.register', compact('sistemas', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->delete()) {
            return redirect()->back()->with('success', 'Usuario Removido Correctamente!');
        }
        
        return redirect()->back()->with('error', 'error al eliminar usuario');
    }

    public function editFav($tipo, $sistema)
    {
        $favSistema = Auth::user()->sistemas()->updateExistingPivot($sistema, ['favorito' => $tipo]);
        return redirect()->back()->with('success', 'Favorito Modificado Correctamente!');
    }
    
    public function deleteUserSistema($sistema)
    {
        $favSistema = Auth::user()->sistemas()->detach($sistema);
        return redirect()->back()->with('success', 'Sistema Removido Correctamente!');
    }

    public function agregarSistemaMasivo(Request $request)
    {
        if ($request->has('sistema') && !is_null($request->sistema)) {
            $users = User::with('userHospitalizado')->whereIn('rut', function ($query) use ($request) {
                $query->select('rut')->from('hospitalizacion.users')->where('perfil_id', $request->id_perfil);
            })->get()->pluck('id');
            $sistemaNuevo = Sistemas::find($request->sistema);
            $sistemaNuevo->users()->detach($users);
            $sistemaNuevo->users()->attach($users);

            return redirect()->back()->with('success', "Se le ha asignado el sistema a {$users->count()} usuarios");
        }

        $sistemas = Sistemas::select('id', 'tx_descripcion')->get()->pluck('tx_descripcion', 'id');
        $perfiles = Perfil::select('id', 'tx_descripcion')->get()->pluck('tx_descripcion', 'id');
        return view('users.sistemaMasivo', compact('sistemas', 'perfiles'));
    }
    
    public function agregarServicioMasivo(Request $request)
    {
        if ($request->has('servicioActual') && !is_null($request->servicioActual)) {
            $users = UserHospitalizados::with('servicios')->whereIn('id', function ($query) use ($request) {
                $query->select('id_usuario')->from('hospitalizacion.hos_usuario_servicio')->where('id_servicio', $request->servicioActual);
            })->get()->pluck('id');
            $sistemaNuevo = Servicio::find($request->servicioNuevo);
            $sistemaNuevo->users()->detach($users);
            $sistemaNuevo->users()->attach($users);

            return redirect()->back()->with('success', "Se le ha asignado el sistema a {$users->count()} usuarios");
        }

        $servicios = Servicio::select('id', 'tx_descripcion')->get()->pluck('tx_descripcion', 'id');
        return view('users.servicioMasivo', compact('servicios'));
    }
}
