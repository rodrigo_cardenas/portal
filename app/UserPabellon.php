<?php

namespace App;
use App\EquipoPabellon;

use Illuminate\Database\Eloquent\Model;

class UserPabellon extends Model
{
    // protected $connection = 'mysql3';
    protected $connection = 'mysql3';
    protected $table = 'cb_usuario';
    public $guarded = [];
    

    // public function setNombreAttribute($value){
    //     $this->attributes['nombre'] = ucfirst($value);
    // }

    public function setGlClaveAttribute($value){
        $this->attributes['gl_clave'] = sha1(intval($value));
    }

    protected $appends = ['medico'];

    public function getMedicoAttribute()
    {
        $rut = explode("-", $this->gl_rut);
        $medico = EquipoPabellon::with('labores')->where('pro_rut', $rut[0])->first();
        return $medico;
    }

    public function Perfiles()
    {
        return $this->belongsToMany('App\PerfilPabellon', 'cb_labor', 'id_usuario', 'id_perfil');
    }

    public function Bodegas()
    {
        return $this->belongsToMany('App\BodegaPabellon', 'cb_usuario_bodega', 'id_usuario', 'id_bodega');
    }

    public function Especialidades()
    {
        return $this->belongsToMany('App\EspecialidadPabellon', 'cb_usuario_especialidad', 'id_usuario', 'id_especialidad');
    }

    public function EquiposMedico()
    {
        return $this->belongsToMany('App\EquipoMedicoPabellon', 'cb_usuario_equipo', 'id_usuario', 'id_equipo_medico');
    }

    public function Pabellones()
    {
        return $this->belongsToMany('App\PabellonPabellon', 'cb_usuario_pabellon', 'id_usuario', 'id_pabellon');
    }
    
    // public function userServicios()
    // {
    //     return $this->hasMany('App\UserServicios', 'id_usuario', 'id_servicio');
    // }
}
