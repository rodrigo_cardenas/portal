<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sistemas extends Model
{
    // protected $connection = 'mysql2';
    protected $table = 'gen_sistemas';

    public function users()
    {
        return $this->belongsToMany('App\User', 'gen_user_sistemas', 'id_sistema', 'id_user')->withPivot('favorito');
    }
}
