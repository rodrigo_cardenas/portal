<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGrupoComite extends Model
{
    protected $connection = 'mysql4';
    protected $table = 'sec_users_groups';
    protected $guarded = [];
    public $timestamps = false;
}
