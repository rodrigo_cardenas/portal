<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BodegaPabellon extends Model
{
    protected $connection = 'mysql3';
    protected $table = 'cb_bodega';
    public $guarded = [];
}
