<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoProfesionPabellon extends Model
{
    protected $connection = 'mysql3';
    protected $table = 'cb_tipo_profesional';
    public $guarded = [];
}
