<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EspecialidadPabellon extends Model
{
    protected $connection = 'mysql3';
    protected $table = 'cb_especialidad';
    public $guarded = [];
}
