<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoProfesionalComite extends Model
{
    protected $connection = 'mysql4';
    protected $table = 'com_tipo_profesional';
}
