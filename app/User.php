<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'nombre', 'rut', 'password', 'password_old',
    // ];

    protected $guarded = [];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot()
    {
    	parent::boot();
       
        static::creating(function($user)
		{
			$user->created_by = Auth::id();
			$user->password_old = bcrypt(intval(substr($user->rut, 0, 4)));
		});
        
        static::created(function($user)
		{
            $user->created_by = Auth::id();
            $user->password_old = bcrypt(intval(substr($user->rut, 0, 4)));
        });
        
        static::deleting(function($user) {
			$user->sistemas()->sync([]);
		});
        
        static::deleted(function($user) {
			$user->sistemas()->sync([]);
		});
	}

    public function setNombreAttribute($value){
        $this->attributes['nombre'] = ucfirst($value);
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = bcrypt($value);
    }

    public function sistemas()
    {
        return $this->belongsToMany('App\Sistemas', 'gen_user_sistemas', 'id_user', 'id_sistema')->withPivot('favorito');
    }

    public function userPortal()
    {
        return $this->hasOne('App\UserPortal', 'rut', 'rut');
    }
    
    public function userHospitalizado()
    {
        return $this->hasOne('App\UserHospitalizados', 'rut', 'rut');
    }

    public function userPabellon()
    {
        return $this->hasOne('App\UserPabellon', 'gl_rut', 'rut');
    }

    public function userComite()
    {
        return $this->hasOne('App\UserComite', 'login', 'rut');
    }

    public function userNotificacion()
    {
        return $this->hasOne('App\UserNotificacion', 'gl_user', 'rut');
    }
}
