<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaborPabellon extends Model
{
    protected $connection = 'mysql3';
    protected $table = 'cb_cod_cirugia_ncr';
    public $guarded = [];
}
