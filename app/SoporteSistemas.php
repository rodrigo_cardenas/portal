<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoporteSistemas extends Model
{
    protected $table = 'soporte_sistema';
    public $timestamps = false;
}