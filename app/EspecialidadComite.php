<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EspecialidadComite extends Model
{
    protected $connection = 'mysql4';
    protected $table = 'com_especialidad';

    protected $appends = ['tx_nombre_minuscula'];

    public function getTxNombreMinusculaAttribute()
    {
        return ucwords(mb_strtolower("{$this->tx_nombre}"));
    }
}
