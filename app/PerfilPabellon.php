<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerfilPabellon extends Model
{
    protected $connection = 'mysql3';
    protected $table = 'cb_perfil';
    public $guarded = [];
}
