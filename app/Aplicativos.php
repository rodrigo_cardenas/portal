<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aplicativos extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'portal.pt_sitio';
    public $guarded = [];
}
