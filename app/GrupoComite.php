<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoComite extends Model
{
    protected $connection = 'mysql4';
    protected $table = 'com_grupo';
}
