<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotificacion extends Model
{
    protected $connection = 'mysql5';
    protected $table = 'cl_usuario';
    public $guarded = [];
    public $timestamps = false;
}
