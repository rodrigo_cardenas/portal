<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anexos extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'anexos.datos';
    public $guarded = [];
}
