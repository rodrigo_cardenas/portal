<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PabellonPabellon extends Model
{
    protected $connection = 'mysql3';
    protected $table = 'cb_pabellon';
    public $guarded = [];
}
