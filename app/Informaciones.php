<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\SoftDeletes;

class Informaciones extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'gen_informaciones';
    public $guarded = [];

    use SoftDeletes;
}
