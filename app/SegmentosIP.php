<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SegmentosIP extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'gen_segmentos_ip';
    protected $appends = ['rango_completo'];

    use SoftDeletes;

    public function getRangoCompletoAttribute()
    {
        return "{$this->ip}/{$this->rango}";
    }
}
