<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstablecimientoComite extends Model
{
    protected $connection = 'mysql4';
    protected $table = 'com_establecimiento';
}
